#pragma once

#include <sure/stack_view.hpp>
#include <sure/trampoline.hpp>

#include <sure/unreachable.hpp>

namespace sure {

// https://refspecs.linuxfoundation.org/abi-eh-1.22.html

namespace exc {

struct Context {
  void* buf[2] = {0, 0};

  void SwitchTo(Context& target);
};

}  // namespace exc

///

template <typename WithSanitizerContext>
class WithExceptions {
  using Context = WithExceptions;

 public:
  WithExceptions() = default;

  void Setup(StackView stack, ITrampoline* trampoline) {
    underlying_.Setup(stack, trampoline);
  }

  void SwitchTo(Context& target) {
    exc_.SwitchTo(target.exc_);
    underlying_.SwitchTo(target.underlying_);
  }

  [[noreturn]] void ExitTo(Context& target) {
    exc_.SwitchTo(target.exc_);
    underlying_.ExitTo(target.underlying_);
    Unreachable();
  }

  void* StackPointer() const noexcept {
    return underlying_.StackPointer();
  }

 public:
  WithSanitizerContext underlying_;
  exc::Context exc_;
};

}  // namespace sure
