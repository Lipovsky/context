#include <sure/exceptions/unix/context.hpp>

#include <stdexcept>
// memcpy
#include <cstring>

namespace __cxxabiv1 {  // NOLINT

struct __cxa_eh_globals;  // NOLINT

// NOLINTNEXTLINE
extern "C" __cxa_eh_globals* __cxa_get_globals() noexcept;

}  // namespace __cxxabiv1

namespace sure::exc {

void Context::SwitchTo(Context& target) {
  static constexpr size_t kSize = sizeof(Context);

  auto* eh = __cxxabiv1::__cxa_get_globals();
  memcpy(buf, eh, kSize);
  memcpy(eh, target.buf, kSize);
}

}  // namespace sure::exc
