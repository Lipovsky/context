#pragma once

#include <sure/stack_view.hpp>
#include <sure/trampoline.hpp>

#include <sure/unreachable.hpp>

#include <sanitizer/tsan_interface.h>

namespace sure {

template <typename MachineContext>
class WithSanitizer final : private ITrampoline {
  using Context = WithSanitizer;

 public:
  WithSanitizer() = default;

  void Setup(StackView stack, ITrampoline* trampoline) {
    user_ = trampoline;
    underlying_.Setup(stack, this);
    fiber_ = __tsan_create_fiber(0);
  }

  void SwitchTo(Context& target) {
    fiber_ = __tsan_get_current_fiber();
    // NB: __tsan_switch_to_fiber should be called immediately before switch to fiber
    // https://github.com/llvm/llvm-project/blob/712dfec1781db8aa92782b98cac5517db548b7f9/compiler-rt/include/sanitizer/tsan_interface.h#L150-L151
    __tsan_switch_to_fiber(target.fiber_, 0);

    underlying_.SwitchTo(target.underlying_);

    AfterSwitch();
  }

  [[noreturn]] void ExitTo(Context& target) {
    target.exit_from_ = this;

    // NB: __tsan_switch_to_fiber should be called immediately before switch to fiber
    // https://github.com/llvm/llvm-project/blob/712dfec1781db8aa92782b98cac5517db548b7f9/compiler-rt/include/sanitizer/tsan_interface.h#L150-L151
    __tsan_switch_to_fiber(target.fiber_, 0);

    underlying_.SwitchTo(target.underlying_);

    Unreachable();
  }

  void* StackPointer() const noexcept {
    return underlying_.StackPointer();
  }

 private:
  // ITrampoline
  void Run() noexcept override {
    AfterSwitch();
    user_->Run();
  }

  void AfterSwitch() {
    if (exit_from_ != nullptr) {
      __tsan_destroy_fiber(exit_from_->fiber_);
      exit_from_ = nullptr;
    }
  }

 private:
  ITrampoline* user_;

  MachineContext underlying_;

  // Thread Sanitizer
  void* fiber_;
  Context* exit_from_{nullptr};
};

}  // namespace sure
