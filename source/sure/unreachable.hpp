#pragma once

#include <cstdlib>

namespace sure {

[[noreturn]] inline void Unreachable() {
  std::abort();
}

}  // namespace sure
