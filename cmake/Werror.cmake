add_compile_options(-Wall -Wextra -Wpedantic -g -fno-omit-frame-pointer)

if(SURE_DEVELOPER)
    # Turn warnings into errors
    add_compile_options(-Werror)
endif()
