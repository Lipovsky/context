ProjectLog("Tests")

# Coroutine

add_executable(sure_tests_coroutine coroutine.cpp)
target_link_libraries(sure_tests_coroutine PRIVATE sure Catch2::Catch2WithMain)

add_test(NAME sure_tests_coroutine COMMAND sure_tests_coroutine)
set_tests_properties(sure_tests_coroutine PROPERTIES LABELS "sure;coroutine")
